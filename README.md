# Dotfiles
A collection of my dotfiles from over the years. These have been battle-
hardened for a while now, so there's likely some inconsistency in some places.
For example, I moved to zsh about a year ago, maybe a bit longer. Since then I
haven't really touched my bashrc. I've also largely moved away from i3 to dwm,
see my headcannon-dwm repo for that.

# Installation
This depends on the program, see their docs for more help.
